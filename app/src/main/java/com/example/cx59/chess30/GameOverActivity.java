package com.example.cx59.chess30;


import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;

/**
 * @author Chuanqi Xiong
 * @author Chenjie Wu
 */

public class GameOverActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gameover_window);

        DisplayMetrics dm = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width* .8), (int)(height *.6));

    }
}
