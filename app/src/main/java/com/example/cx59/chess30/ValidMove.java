package com.example.cx59.chess30;


import android.util.Log;

/**
 * This class is to check if a move is valid or not
 * @author Chuanqi Xiong
 * @author Chenjie Wu
 */
public class ValidMove {

    /**
     * Method to check whether the move of a target is valid or not.
     * @param target: Piece target
     * @param move: the move position
     * @return the move is true/false
     */
    public boolean checkmove(Piece target, String move) {

        int vertical_displace = Math.abs(move.charAt(1) - move.charAt(4));
        int horizontal_displace = Math.abs(move.charAt(0) - move.charAt(3));

        //Rules to move Rook: Up and down or sideways
        if(target instanceof Rook) {
            return vertical_displace == 0 || horizontal_displace == 0;
        }

        //Rules to move knight: 2 vertical 1 horizontal, or the other way around
        if(target instanceof Knight) {

            if(vertical_displace == 1 && horizontal_displace == 2) {
                return true;
            }
            return vertical_displace == 2 && horizontal_displace == 1;
        }

        //Rules to move Bishop: only diagonal
        if(target instanceof Bishop) {
            return vertical_displace == horizontal_displace;
        }

        //Rules to move pawn: one at a time
        if(target instanceof Pawn) {
            String end_pos = move.substring(3,5);
            //first check if the pawn is going backwards
            if(move.charAt(4) - move.charAt(1) > 0 && target.color.equals("b")){
                return false; //A black pawn can only go down
            }
            if(move.charAt(4) - move.charAt(1) < 0 && target.color.equals("w")){
                return false; //A white pawn can only go up
            }

            //regular move
            if(vertical_displace == 1 && horizontal_displace == 0) {
                return Utilities.get_piece(end_pos) == null;
            }
            //two squares in first move
            if(target.steps == 0 && (vertical_displace == 2 && horizontal_displace == 0)) {
                //target.steps++;
                return true;
            }
            //Capturing piece
            if(vertical_displace == 1 && horizontal_displace == 1) {
                //Regular capture

                if(Utilities.get_piece(end_pos) != null) {
                    if(! Utilities.get_piece(end_pos).color.equals(target.color)) {
                        return true; //You can only capture the opponent's piece
                    }
                }

                //En Passant
                String EPtarget;

                //When white en passant black
                if(target.color.equals("w")){
                    EPtarget = "" + move.charAt(3) + (Character.getNumericValue(move.charAt(4)-1));
                }
                //When black en passant white
                else EPtarget = "" + move.charAt(3) + (Character.getNumericValue(move.charAt(4)+1));
                if(Utilities.get_piece(EPtarget) != null) {
                    if((Utilities.get_piece(EPtarget) instanceof Pawn) && (Utilities.get_piece(EPtarget).steps == 1)) {
                        Utilities.capture_piece(Utilities.get_piece(EPtarget));
                        return true;
                    }
                }
            }
            return false;
        }

        if (target instanceof Queen)
        {
            if(vertical_displace == 0 || horizontal_displace == 0)
                return true;

            return vertical_displace == horizontal_displace;
        }


        if (target instanceof King)
        {
            Log.d("You are moving a King", "");
            if(vertical_displace == horizontal_displace && vertical_displace == 1)
                return true;
            
            //when moving along rank or file
            if((vertical_displace == 0 || horizontal_displace == 0) && (vertical_displace == 1 || horizontal_displace == 1))
                return true;

            //check for castling
            if(horizontal_displace == 2 && vertical_displace == 0) {
                if(target.steps == 0) {
                    switch(move) {
                        //white king-side castling
                        case "e1 g1":
                            if(Chess.playerW[7].steps == 0) {
                                return true;
                            }
                            break;

                        //black king-side castling
                        case "e8 g8":
                            if(Chess.playerB[7].steps == 0) {
                                return true;
                            }
                            break;

                        //white queen-side castling
                        case "e1 c1":
                            if(Chess.playerW[0].steps == 0) {
                                return true;
                            }
                            break;

                        //black queen-side castling
                        case "e8 c8":
                            if(Chess.playerB[0].steps == 0) {
                                return true;
                            }
                            break;

                        default:
                            return false;

                    }
                }
            }
            return false;
        }
        System.out.println("Oops...");
        return false;
    }
}
