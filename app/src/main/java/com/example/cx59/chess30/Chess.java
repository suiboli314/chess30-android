package com.example.cx59.chess30;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chuanqi Xiong
 * @author Chenjie Wu
 */

public class Chess{
    static Piece[] playerW;
    static Piece[] playerB;

    static char round = 'w';

    static public void chess_init(){

        playerW = Utilities.initPlayer("w");
        playerB = Utilities.initPlayer("b");

        round = 'w';
    }

    static public String move(String move){

        char promotion = ' ';
        String curr_pos, end_pos;

        if(move.contains("draw")){
            return "draw";
        }
        else if(move.contains("resign")){
            return "resign";
        }

        // Pawn promotion
        if((move.length() == 7) && ((move.charAt(6) == 'R') || (move.charAt(6) == 'N')||(move.charAt(6) == 'B')||(move.charAt(6) == 'Q'))){
            promotion = move.charAt(6);
        }

        //Invalid command
        else if(move.length() != 5){
            return "invalid";
        }

        curr_pos = String.valueOf(move.substring(0,2));
        end_pos = String.valueOf(move.substring(3,5));

        //Trying to capture your own piece
        if((get_piece(curr_pos) != null) && (get_piece(end_pos) != null)){
            if(get_piece(curr_pos).toString().charAt(0) == get_piece(end_pos).toString().charAt(0)){

                return "invalid";
            }
        }

        //Check if the move is valid
        ValidMove check = new ValidMove();
        if(!(check.checkmove(get_piece(curr_pos), move))) {
            return "invalid";
        }

        //Check if it is the right round
        if(get_piece(curr_pos).toString().charAt(0) != round){
            Log.d("Warning: ", "You are moving the opponent's piece");
            return "invalid";
        }

        //If the piece is not a Knight, check if the path is clear
        if(! (get_piece(curr_pos) instanceof Knight)) {
            Log.d("the piece is ", curr_pos);
            if(!(get_piece(curr_pos).isClearPath(move))) {
                return "invalid";
            }
        }

        // See if its castling
        if((move.equals("e1 g1") || move.equals("e1 c1")) || (move.equals("e8 g8") || move.equals("e8 c8"))) {
            switch(move) {
                //white king-side castling
                case "e1 g1":
                    if(Chess.playerW[7].steps == 0) {
                        playerW[7].updatePos("f1");
                        playerW[4].steps++;
                    }
                    break;

                //black king-side castling
                case "e8 g8":
                    if(Chess.playerB[7].steps == 0) {
                        playerB[7].updatePos("f8");
                        playerB[4].steps++;
                    }
                    break;

                //white queen-side castling
                case "e1 c1":
                    if(Chess.playerW[0].steps == 0) {
                        playerW[0].updatePos("d1");
                        playerW[4].steps++;
                    }
                    break;

                //black queen-side castling
                case "e8 c8":
                    if(Chess.playerB[0].steps == 0) {
                        playerB[0].updatePos("d8");
                        playerB[4].steps++;
                        //return true;
                    }
            }
        }

        Piece capture_king = get_piece(end_pos);
        if((get_piece(end_pos))!= null){
            capture_piece(get_piece(end_pos));

            if(capture_king instanceof King)
            {
                System.out.println("get capture king!!");
                if(capture_king.color.equals("w"))
                {
                    return "Black Won";
                }
                else
                    return "White Won";
            }

        }

		/* Pawn Promotion
        if(get_piece(curr_pos) instanceof Pawn) {
            //Piece promote = promotion(get_piece(curr_pos), end_pos, promotion);
        }*/

        get_piece(curr_pos).updatePos(end_pos);
        get_piece(end_pos).steps++;

        char checkcheck = testincheck();

        if(checkcheck != 'n'){
            String checkmate = checkMate(checkcheck);
            if(!checkmate.equals("nah")){
                return checkmate;
            }
            swap_round();
            return checkcheck+"";
        }

        swap_round();
        return "good";
    }
    public static void swap_round(){
        if(round == 'w'){ round = 'b'; return;}
        if(round == 'b'){ round = 'w'; return;}
    }
    /**
     * Method that returns a piece of the given position
     * @param pos: the current position
     * @return get the current piece
     *
     */
    public static Piece get_piece(String pos) {

        for(int i = 0; i < 16; i++) {

            if (playerW[i].pos.equals(pos)) {
                return playerW[i];
            }
            if (playerB[i].pos.equals(pos)) {
                return playerB[i];
            }
        }
        Log.d("get_piece: Warning", "No Piece At All");
        return null;
    }
    public static void capture_piece(Piece target) {
        target.updatePos("z9"); //move the target out of the board
    }

    public static String[] all_moves(Piece target) {
        String[] result = new String[64];
        char rank, file;
        int i = 0;
        //run through all squares on a board and add those are valid for the target piece

        for(file = '1'; file <= '8'; file++) {
            for(rank = 'a'; rank <= 'h'; rank++) {

                ValidMove check = new ValidMove();

                String curr_position = Character.toString(rank) + Character.toString(file);
                if(target instanceof Pawn) {

                    continue;
                }
                if(check.checkmove(target, target.pos + " " + curr_position)) {
                    if(! (target instanceof Knight)) {
                        if(target.isClearPath(target.pos + " " + curr_position)) {
                            result[i++] = Character.toString(rank) + Character.toString(file);
                        }
                    }
                    else if (target instanceof Knight) {
                        result[i++] = Character.toString(rank) + Character.toString(file);
                    }
                }

            }
        }

        return result;
    }

    public static Piece[] square_threatened(String square) {
        Piece result[] = new Piece[32];
        int result_i = 0;
        for(int i = 0; i < 16; i ++) {
            String possible_square_w[] = all_moves(playerW[i]);
            String possible_square_b[] = all_moves(playerB[i]);
            for(int j = 0; j < 64; j++) {
                if(square.equals(possible_square_w[j])) {
                    result[result_i++] = playerW[i];
                }
                if(square.equals(possible_square_b[j])) {
                    result[result_i++] = playerB[i];
                }
            }
        }

        //check if there is a threatened pawn
        char x = square.charAt(0);
        char y = square.charAt(1);

        //check the bottom left square
        if(get_piece(Character.toString((char) (x-1)) + Character.toString((char)(y-1))) instanceof Pawn) {
            result[result_i++] = get_piece(Character.toString((char) (x-1)) + Character.toString((char)(y-1)));
        }

        //check the top left square
        if(get_piece(Character.toString((char) (x-1)) + Character.toString((char)(y+1))) instanceof Pawn) {
            result[result_i++] = get_piece(Character.toString((char) (x-1)) + Character.toString((char)(y+1)));
        }

        //check the bottom right square
        if(get_piece(Character.toString((char) (x+1)) + Character.toString((char)(y-1))) instanceof Pawn) {
            result[result_i++] = get_piece(Character.toString((char) (x+1)) + Character.toString((char)(y-1)));
        }

        //check the top right square
        if(get_piece(Character.toString((char) (x+1)) + Character.toString((char)(y+1))) instanceof Pawn) {
            result[result_i++] = get_piece(Character.toString((char) (x+1)) + Character.toString((char)(y+1)));
        }
        return result;
    }

    private static char testincheck() {
        Piece attack_b[] = square_threatened(playerW[4].pos);
        Piece attack_w[] = square_threatened(playerB[4].pos);

        for(int i = 0; i < 32; i++) {
            if(attack_b[i] != null) {
                if(attack_b[i].color.equals("b")) {
                    System.out.println("attacking: " + attack_b[i].toString() + " in " + attack_b[i].pos);
                    System.out.println("\nwhite in check");
                    return 'w';
                }
            }
            if(attack_w[i] != null) {
                if(attack_w[i].color.equals("w")) {
                    System.out.println("attacking: " + attack_w[i].toString() + " in " + attack_w[i].pos);
                    System.out.println("\nblack in check");
                    return 'b';
                }
            }
        }

        return 'n';
    }
    //char color is the color under a check
    private static String checkMate(char color){
        // Check which king is under a check
        String king_pos;
        if(color == 'w'){
            king_pos = playerW[4].pos;
        }
        else{
            king_pos = playerB[4].pos;
        }

        // Check if the surrounding square are threatened
        List<String> surr = get_surroundings(king_pos);

        Log.d("Now in checkMate()", color + " is being tested");
        Log.d("King's position is ", king_pos);
        for(String temp: surr){
            Log.d("Surroundings are: ", temp);
        }

        int possible_way_out = surr.size();
        for(String pos: surr){
            Piece attacking_pieces_temp[] = square_threatened(pos);
            List<Piece> attack_pieces = attack_screening(attacking_pieces_temp, color);
            for(Piece temp: attack_pieces){
                Log.d("----------", temp.toString() + " at " + temp.pos + " is attacking " + pos);
                possible_way_out--;
            }
        }
        if(possible_way_out == 0){
            Log.d("No way out", " Have to kill the target");
        }

        if(possible_way_out > 0){
            Log.d("There is way out", " No worries");
            return "nah";
        }

        List<Piece> fatal_pieces = attack_screening(square_threatened(king_pos), color);

        if(fatal_pieces.size() == 1){
            List<Piece> ct_attack = attack_screening(square_threatened(fatal_pieces.get(0).pos), reverse_color(color));
            if(ct_attack.size() > 0){
                return "nah";
            }
        }

        if(color == 'w'){
            return "Black Won";
        }
        else
            return "White Won";
    }

    //Return all surrounding square around pos
    public static List<String> get_surroundings(String pos){
        List<String> surr = new ArrayList<>();
        char row = pos.charAt(0);
        char col = pos.charAt(1);
        surr.add(Character.toString(((char)(row - 1))) + Character.toString((char)(col+1)));
        surr.add(Character.toString(((char)(row + 0))) + Character.toString((char)(col+1)));
        surr.add(Character.toString(((char)(row + 1))) + Character.toString((char)(col+1)));
        surr.add(Character.toString(((char)(row + 1))) + Character.toString((char)(col+0)));
        surr.add(Character.toString(((char)(row + 1))) + Character.toString((char)(col-1)));
        surr.add(Character.toString(((char)(row - 0))) + Character.toString((char)(col-1)));
        surr.add(Character.toString(((char)(row - 1))) + Character.toString((char)(col-1)));
        surr.add(Character.toString(((char)(row - 1))) + Character.toString((char)(col-0)));

        List<String> newList = new ArrayList<>();
        List<String> pos_occupied = new ArrayList<>();

        for(int i = 0; i < 16; i++){
            pos_occupied.add(playerB[i].pos);
            pos_occupied.add(playerW[i].pos);
        }
        for(String temp: surr){
            //Check if rank out of bounds
            if((temp.charAt(1) >= '1' && temp.charAt(1) <= '8') && (temp.charAt(0) >= 'a' && temp.charAt(0) <= 'h')) {

                if(!pos_occupied.contains(temp)){
                    newList.add(temp);
                }
            }
        }
        return newList;
    }

    static public Piece clone_piece(Piece target){
        Piece temp;
        if(target instanceof Pawn){
            temp = new Pawn();
            temp.pos = target.pos;
            temp.color = target.color;
            temp.steps = target.steps;
            return temp;
        }
        if(target instanceof Rook){
            temp = new Rook();
            temp.pos = target.pos;
            temp.color = target.color;
            temp.steps = target.steps;
            return temp;
        }
        if(target instanceof Knight){
            temp = new Knight();
            temp.pos = target.pos;
            temp.color = target.color;
            temp.steps = target.steps;
            return temp;
        }
        if(target instanceof Bishop){
            temp = new Bishop();
            temp.pos = target.pos;
            temp.color = target.color;
            temp.steps = target.steps;
            return temp;
        }
        if(target instanceof Queen){
            temp = new Queen();
            temp.pos = target.pos;
            temp.color = target.color;
            temp.steps = target.steps;
            return temp;
        }
        if(target instanceof King){
            temp = new King();
            temp.pos = target.pos;
            temp.color = target.color;
            temp.steps = target.steps;
            return temp;
        }
        else{
            System.out.println("Error in clone_piece");
        }

        return null;
    }

    static public Piece[] clone_piece_arr(Piece[] target){
        Piece[] result = new Piece[16];

        for(int i = 0; i < 16; i++){
            result[i] = Chess.clone_piece(target[i]);
        }
        return result;
    }

    static public List<Piece> attack_screening(Piece[] pieces, char color){
        List<Piece> result = new ArrayList<>();

        for(Piece temp: pieces){
            if(temp == null){
                continue;
            }
            if(temp.toString().charAt(0) != color){
                result.add(temp);
            }
        }
        return result;
    }

    static private char reverse_color(char color){
        if(color == 'w'){
            return 'b';
        }else return 'w';
    }
}
