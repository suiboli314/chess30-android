package com.example.cx59.chess30;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Chuanqi Xiong
 * @author Chenjie Wu
 */

public class ChessActivity extends Activity{

    static Button RESIGN;
    static Button UNDO;
    static Button NEXT;
    static Button REPLAY;
    static Button AI;
    static Button DRAW;
    static ToggleButton viewWhite;
    static ToggleButton viewBlack;
    static TableLayout the_board;
    Piece[] tempW;
    Piece[] tempB;

    static boolean can_undo = true;

    int round_count = 0;
    static ImageView[] chessBoard = new ImageView[64];

    static List<String> aGame;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        initialize();
        sync_board();
    }

    public void initialize()
    {
        aGame = new ArrayList<>();

        setContentView(R.layout.activity_main);

        the_board = findViewById(R.id.the_board);
        //the_board.setRotation((float) 180);

        Resources res = getApplicationContext().getResources(); //if you are in an activity

        NEXT = findViewById(R.id.next_move);
        NEXT.setEnabled(false);


        UNDO = findViewById(R.id.undo_button);
        UNDO.setEnabled(false);
        UNDO.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.d("BUTTON", "You are clicking UNDO");
                undo();
            }
        });

        AI = findViewById(R.id.AI);
        AI.setEnabled(true);
        AI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("BUTTON", "You are using AI");
                AI_move();
            }
        });

        RESIGN = findViewById(R.id.resign_button);
        RESIGN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("BUTTON", "You are using RESIGN");
                resign();
            }
        });
        RESIGN.setEnabled(true);

        REPLAY = findViewById(R.id.replay);
        REPLAY.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.d("BUTTON", "You are clicking UNDO");
                replay_list();
                for(ChessGame temp: MainActivity.GameList){
                    //System.out.println("666666666666666");
                    for(String temp2: temp.games){
                        //System.out.println(temp2);
                    }
                }
            }
        });


        Button draw = findViewById(R.id.draw_button);

        DRAW = draw;
        draw.setEnabled(true);
        draw.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.d("BUTTON", "You are clicking DRAW");
                draw();
            }
        });

        char rows = 'a';
        int columns = 1;
        for (int i = 0; i < 64; i++) {
            if(columns > 8){
                columns = 1;
                rows++;
            }
            if(rows > 'h'){
                break;
            }
            String idName = (rows + "") + (columns++ + "");
            chessBoard[i] = findViewById(res.getIdentifier(idName, "id", getPackageName()));
            chessBoard[i].setOnTouchListener(new ChoiceTouchListener());
            chessBoard[i].setOnDragListener(new ChoiceDragListener());
        }

        Chess.chess_init();
        sync_board();

    }

    final class ChoiceTouchListener implements View.OnTouchListener
    {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if ((event.getAction() == MotionEvent.ACTION_DOWN) && ((ImageView) v).getDrawable() != null) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                v.startDrag(data, shadowBuilder, v, 0);
                return true;
            }
            else
                return false;

        }
    }

    final class ChoiceDragListener implements View.OnDragListener
    {
        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:

                    tempW = Chess.clone_piece_arr(Chess.playerW);
                    tempB = Chess.clone_piece_arr(Chess.playerB);

                    //no action necessary
                    ImageView view = (ImageView) event.getLocalState();//the source image
                    String fromPos = imageViewToPos(view);
                    String toPos = imageViewToPos((ImageView)v);
                    String route = fromPos + " " + toPos;
                    String result = Chess.move(route);

                    if(result.equals("invalid")){
                        Log.d("Warning: ", "the move is invalid");
                        break;
                    }

                    UNDO.setEnabled(true);
                    aGame.add(route);

                    //System.out.println("******************");
                    for(String temp: aGame){
                        System.out.println(temp);
                    }
                    if(result.contains("Won")){
                        game_over(result);
                    }
                    //Black is being checked
                    if(result.equals("b")){
                        Log.d("check msg: ", "black is being checked");
                        Toast toast = Toast.makeText(getApplicationContext(), "Black is being checked",
                                Toast.LENGTH_LONG);
                        //toast.setGravity(Gravity.TOP|Gravity.LEFT, 0, 0);
                        toast.show();
                    }
                    //White is being checked
                    if(result.equals("w")){
                        Log.d("check msg: ", "white is being checked");
                        Toast toast = Toast.makeText(getApplicationContext(), "White is being checked",
                                Toast.LENGTH_LONG);
                        //toast.setGravity(Gravity.TOP|Gravity.LEFT, 0, 0);
                        toast.show();
                    }
                    Log.d("route: ", Utilities.move_piece(route));
                    ((ImageView)v).setImageDrawable(view.getDrawable());

                    if(view.equals(v)){
                        Log.d("Do Nothing:", "The piece did not move");
                        break;
                    }

                    view.setImageDrawable(null); //replace the source by empty
                    can_undo = true;
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    sync_board();
                    //no action necessary
                    break;
            }
            //sync_board();
            //whereArePawns();
            return true;
        }
    }
//
//    static public void whereArePawns(){
//        for(int i = 8; i < 16; i++){
//            //Log.d("pawn at ", Utilities.playerW[i].pos);
//        }
//    }


    public String imageViewToPos(ImageView img)
    {
        char rows = 'a';
        int columns = 1;

        for (int i = 0; i < 64; i++)
        {
            if (columns > 8) {
                columns = 1;
                rows++;
            }

            if (rows > 'h')
                return null;

            if (img.getId() == chessBoard[i].getId())
                return  (rows+"") + (columns+"");

            columns++;
        }

        return null;
    }

    public void sync_board()
    {
        for(int i = 0; i < 64; i ++)
        {
            String position = Utilities.intToPos(i);
            ImageView temp = findViewById(getResources().
                    getIdentifier(position, "id", getPackageName()));

            //Log.d("checking pos ", position);
            if(Utilities.get_piece(Utilities.intToPos(i)) != null){
                //Log.d("in sync_board", position + "is not empty");
                setPieceImage(temp, Utilities.get_piece(Utilities.intToPos(i)).toString());
            }
            else {
                //Log.d("in sync_board", position + "is empty");
                temp.setImageDrawable(null);
            }

        }

    }

    public void setPieceImage(ImageView img, String pieceToStr)
    {
        if(pieceToStr.equals("bB")){img.setImageDrawable(getResources().getDrawable(R.drawable.bb));}
        if(pieceToStr.equals("bK")){img.setImageDrawable(getResources().getDrawable(R.drawable.bk));}
        if(pieceToStr.equals("bN")){img.setImageDrawable(getResources().getDrawable(R.drawable.bn));}
        if(pieceToStr.equals("bP")){img.setImageDrawable(getResources().getDrawable(R.drawable.bp));}
        if(pieceToStr.equals("bQ")){img.setImageDrawable(getResources().getDrawable(R.drawable.bq));}
        if(pieceToStr.equals("bR")){img.setImageDrawable(getResources().getDrawable(R.drawable.br));}
        if(pieceToStr.equals("wB")){img.setImageDrawable(getResources().getDrawable(R.drawable.wb));}
        if(pieceToStr.equals("wK")){img.setImageDrawable(getResources().getDrawable(R.drawable.wk));}
        if(pieceToStr.equals("wN")){img.setImageDrawable(getResources().getDrawable(R.drawable.wn));}
        if(pieceToStr.equals("wP")){img.setImageDrawable(getResources().getDrawable(R.drawable.wp));}
        if(pieceToStr.equals("wQ")){img.setImageDrawable(getResources().getDrawable(R.drawable.wq));}
        if(pieceToStr.equals("wR")){img.setImageDrawable(getResources().getDrawable(R.drawable.wr));}

    }

    public void undo()
    {
        if(!can_undo)
            return;

        Utilities.playerW = tempW;
        Utilities.playerB = tempB;

        Chess.playerW = tempW;
        Chess.playerB = tempB;

        for(Piece temp: Utilities.playerW){
            System.out.println(temp.pos + " is " + temp.toString());
        }
        System.out.println("called sync_board in undo()");
        //Log.d("", "called sync_board in undo()");
        sync_board();
        aGame.remove(aGame.size()-1);
        Chess.swap_round();
        can_undo = false;
        UNDO.setEnabled(false);
    }

    public void draw()
    {
        Toast toast = Toast.makeText(getApplicationContext(), "Draw!",
                Toast.LENGTH_LONG);
        toast.show();

        game_over("DRAW");
    }

    public void game_over(String game_result)
    {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ChessActivity.this);

        View mView = getLayoutInflater().inflate(R.layout.gameover_window, null);

        final EditText mGamename = mView.findViewById(R.id.game_over_text);

        if(game_result.contains("replay end")){
            mGamename.setEnabled(false);
        }
        else{
            mGamename.setEnabled(true);
        }

        TextView title = mView.findViewById(R.id.game_over_title);
        title.setText("Game Over: " + game_result);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        Button record_btn = mView.findViewById(R.id.record_button);

        record_btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(!mGamename.getText().toString().isEmpty()){

                    Toast.makeText(ChessActivity.this,
                            "Record Successfull",
                            Toast.LENGTH_SHORT).show();
                    save_game(mGamename.getText().toString());
                    initialize();
                    print_game();
                    dialog.cancel();
                }else{
                    Toast.makeText(ChessActivity.this,
                            "Please fill in the game name",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
        Button cancel_record = mView.findViewById(R.id.cancel_record);

        cancel_record.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Toast.makeText(ChessActivity.this,
                        "Cancel Record",
                        Toast.LENGTH_SHORT).show();
                initialize();
                dialog.cancel();
            }
        });


    }

    public void replay_list()
    {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ChessActivity.this);

        View mView = getLayoutInflater().inflate(R.layout.gamelist_window, null);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        Button record_btn = mView.findViewById(R.id.record_button);

        viewWhite = mView.findViewById(R.id.view_white);

        viewBlack = mView.findViewById(R.id.view_black);

        viewWhite.setChecked(true);
        viewBlack.setChecked(false);

        viewWhite.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if(viewBlack.isChecked()){
                    viewBlack.setChecked(false);
                }
                else
                    viewBlack.setChecked(true);
            }
        });
        viewBlack.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if(viewWhite.isChecked()){
                    viewWhite.setChecked(false);
                }
                else
                    viewWhite.setChecked(true);
            }
        });


        //Button replay_button = (Button) mView.findViewById(R.id.replay_button);
        Button cancel_replay = mView.findViewById(R.id.cancel_replay);

        ListView gameList = mView.findViewById(R.id.game_list);

        ListAdapter listad = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, form_list());

        gameList.setAdapter(listad);

        gameList.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String game_name = String.valueOf(adapterView.getItemAtPosition(i));

                        dialog.cancel();

                        replay_game(game_name);
                    }
                }
        );

        cancel_replay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Toast.makeText(ChessActivity.this,
                        "Cancel Replay",
                        Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
    }

    public void print_game()
    {
        for(ChessGame temp: MainActivity.GameList){
            System.out.println("*************");
            for(String temp1: temp.games){
                System.out.println(temp1);
            }
        }
    }

    public void save_game(String gameName)
    {
        //Save the title of the game to the last element of the list
        if(aGame.size()<1){
            Log.d("Error: ", "Cannot save an empty game");
            //return;
        }

        aGame.add(gameName);
        addToGames(aGame, MainActivity.GameList);

    }

    public String[] form_list()
    {
        String[] result = new String[MainActivity.GameList.size()];
        List<String> result_temp = new ArrayList<>();

        int i = 0;
        for(ChessGame temp: MainActivity.GameList){
            //System.out.println("******************");
            for(String test: temp.games){
                //System.out.println(test);
            }
            result_temp.add(temp.games.get(temp.games.size()-1) + " |TIME " + temp.game_time);
            //result[i++] = temp.games.get(temp.games.size()-1) + " |TIME " + temp.game_time;
        }
        Collections.sort(result_temp);
        for(String temp: result_temp){
            result[i++] = temp;
        }
        return result;
    }

    public void replay_game(String name)
    {
        initialize();

        if(viewBlack.isChecked()){
            Log.d("Attention: ", "Flip the board");
            //the_board = findViewById(R.id.the_board);
            the_board.setRotation((float) 180);
        }

        NEXT.setEnabled(true);
        UNDO.setEnabled(false);
        AI.setEnabled(false);
        DRAW.setEnabled(false);
        RESIGN.setEnabled(false);

        List<String> theGame = new ArrayList<>();
        for(ChessGame check_game: MainActivity.GameList){
            Log.d("game name: " + check_game.games.get(check_game.games.size()-1), "time saved: " + check_game.getTime());
            if(check_game.toString().equals(name)){
                theGame = check_game.games;
            }
        }

        final List<String> game_final = theGame;

        if(theGame.size()<1){
            Log.d("Warning","the game selected does not exist");
            return;
        }

        Button next_move = findViewById(R.id.next_move);

        NEXT = next_move;

        for(String moves: theGame){
            System.out.println(moves);
        }

        next_move.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //System.out.println("You are implicitly moving: " + move_temp);
                play_game(game_final);
                sync_board();
            }
        });


    }

    public void play_game(List<String> game)
    {
        String result = Chess.move(game.get(round_count++));
        for(Piece temp: Chess.playerW){
            System.out.println(temp.toString() + " " + temp.pos);
        }
        sync_board();

        System.out.println(game.size());
        System.out.println(round_count);
        if(game.size() == round_count){
            System.out.println("reached here");
            game_over("replay end");
            round_count = 0;
            initialize();
        }
    }

    public void addToGames(List<String> onegame, List<ChessGame> gamelist)
    {
        //List<String> anewlist = new ArrayList<>();
        ChessGame a_game = new ChessGame();
        a_game.initGame();
        a_game.setGameName(onegame.get(onegame.size()-1));

        for(String temp: onegame){
            a_game.games.add(temp);
        }
        gamelist.add(a_game);
    }

    public void AI_move()
    {
        Random rand = new Random();
        boolean testInvalid = true;
        String result = null;
        while(testInvalid){
            String start_pos = Utilities.intToPos(rand.nextInt(64));
            String end_pos = Utilities.intToPos(rand.nextInt(64));

            if(!Chess.move(start_pos + " " + end_pos).equals("invalid")){
                testInvalid = false;
                result = start_pos + " " + end_pos;
            }
        }

        aGame.add(result);
        sync_board();
    }

    public void resign()
    {
        if(Chess.round == 'w'){
            game_over("Black Wins!");
        }
        else{
            game_over("White Wins!");
        }
    }


}
