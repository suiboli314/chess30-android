package com.example.cx59.chess30;


import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Chuanqi Xiong
 * @author Chenjie Wu
 */

public class ChessGame {

    String game_name = null;
    String game_time = null;
    public List<String> games;

    public void initGame(){
        games = new ArrayList<>();
        game_time = setTime();
    }

    public void setGameName(String game_name){
        this.game_name = game_name;
    }

    public String getName(){
        return this.game_name;
    }

    public String setTime(){
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy: hh-mm-ss a");

        game_time = sdf.format(date);

        Log.d("in ChessGame: ", "saving game time..." + game_time);
        return game_time;
    }

    public String getTime(){
        return game_time;
    }

    public String toString(){
        return this.game_name + " |TIME " + this.game_time;
    }
}
