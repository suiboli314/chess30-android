package com.example.cx59.chess30;

/**
 * @author Chuanqi Xiong
 * @author Chenjie Wu
 */

abstract class Piece
{
    String pos;
    String color = "w";
    int steps = 0;

    /**
     * Method to decide whether the moving path is valid or not
     * @param move the String move position
     * @return Boolean true/false
     */
    public boolean isClearPath(String move)
    {
        int posx = columnNum(move.charAt(0));
        int posy = rowNum(move.charAt(1));
        int movex = columnNum(move.charAt(3));
        int movey = rowNum(move.charAt(4));
        String curPos = "";

        // same col
        if (posx == movex && posy != movey)
        {
            int curr = Math.min(posy, movey) + 1;
            int end = Math.max(posy, movey) - 1;

            while (curr <= end) {
                curPos = columnseq(posx) + Integer.toString(rowSeq(curr)) ;
                if (Utilities.get_piece(curPos) != null)
                    return false;

                curPos = "";
                curr++;
            }
            return true;
        }
        // same row
        else if (posx != movex && posy == movey)
        {
            int curr = Math.min(posx, movex) + 1;
            int end = Math.max(posx, movex) - 1;

            while (curr <= end) {
                curPos = columnseq(curr) + Integer.toString(rowSeq(posy));
                if (Utilities.get_piece(curPos) != null)
                    return false;

                curPos = "";
                curr++;
            }
            return true;
        }
        // dia
        else if (Math.abs(posx - movex) == Math.abs(posy - movey))
        {
            int dx = (posx < movex) ? 1 : -1;
            int dy = (posy < movey) ? 1 : -1;
            int currX = posx + dx;
            int currY = posy + dy;

            while ((dx == 1 && currX < movex) || (dx == -1 && currX > movex)){
                curPos = columnseq(currX) + Integer.toString(rowSeq(currY));
                if (Utilities.get_piece(curPos) != null)
                    return false;

                curPos = "";
                currX += dx;
                currY += dy;
            }
            return true;
        }
        return false;
    }

    /**
     * Method to update the current position of piece
     * @param pos: position
     * @return String pos
     */
    public String updatePos(String pos)
    {
        this.pos = pos;
        return this.pos;
    }

    /**
     * Method to transform the Character to Integer
     * @param column the Character column
     * @return	Integer Number
     */
    private int columnNum(char column)
    {
        switch (column) {
            case 'a':
                return 0;
            case 'b':
                return 1;
            case 'c':
                return 2;
            case 'd':
                return 3;
            case 'e':
                return 4;
            case 'f':
                return 5;
            case 'g':
                return 6;
            case 'h':
                return 7;
            default:
                return -1;
        }
    }

    /**
     * Method to transfer the column number to its corresponding character
     * @param column the column number
     * @return the corresponding column
     */
    private char columnseq(int column)
    {
        switch (column) {
            case 0:
                return 'a';
            case 1:
                return 'b';
            case 2:
                return 'c';
            case 3:
                return 'd';
            case 4:
                return 'e';
            case 5:
                return 'f';
            case 6:
                return 'g';
            case 7:
                return 'h';
            default:
                return '-'+'1';
        }
    }

    /**
     * Method to change the row number of piece
     * @param row the row number
     * @return Integer row number
     */
    private int rowNum(char row)
    {
        switch (row) {
            case '0':
                return 8;
            case '1':
                return 7;
            case '2':
                return 6;
            case '3':
                return 5;
            case '4':
                return 4;
            case '5':
                return 3;
            case '6':
                return 2;
            case '7':
                return 1;
            case '8':
                return 0;
            default:
                return -1;
        }
    }

    /**
     * Method to change the sequence of row number
     * @param row Integer row number
     * @return Integer row number
     */
    private int rowSeq(int row)
    {
        switch (row) {
            case 0:
                return 8;
            case 1:
                return 7;
            case 2:
                return 6;
            case 3:
                return 5;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 1;
            case 8:
                return 0;
            default:
                return -1;
        }
    }

}

//Private class Rook extends from Piece
class Rook extends Piece{
    public String toString() {
        return color + "R";
    }
}

//Private class Knight extends from Piece
class Knight extends Piece{
    public String toString() {
        return color + "N";
    }
}

//Private class Bishop extends from Piece
class Bishop extends Piece{
    public String toString() {
        return color + "B";
    }
}

//Private class King extends from Piece
class King extends Piece{
    public String toString() {
        return color + "K";
    }
}

//Private class Queen extends from Piece
class Queen extends Piece{
    public String toString() {
        return color + "Q";
    }
}

//Private class Pawn extends from Piece
class Pawn extends Piece{
    public String toString() {
        return color + "P";
    }
}

