package com.example.cx59.chess30;


import android.util.Log;

/**
 * This Class handles all of the functions of a chess game
 * @author Chuanqi Xiong
 * @author Chenjie Wu
 */

public class Utilities {
    //create 2 players
    public static Piece[] playerW = Chess.playerW;
    public static Piece[] playerB = Chess.playerB;

    /**
     * The method to initialize for each player:
     * 1. Set up new pieces
     * 2. Assign color for all pieces
     * 3. Assign initial position for each piece
     * @param color: the color of the Player
     * @return an array of piece
     */
    public static Piece[] initPlayer(String color) {
        Piece[] chessList = new Piece[16];
        //Init pieces
        chessList[0] = new Rook();
        chessList[1] = new Knight();
        chessList[2] = new Bishop();
        chessList[3] = new Queen();
        chessList[4] = new King();
        chessList[5] = new Bishop();
        chessList[6] = new Knight();
        chessList[7] = new Rook();
        chessList[8] = new Pawn();
        chessList[9] = new Pawn();
        chessList[10] = new Pawn();
        chessList[11] = new Pawn();
        chessList[12] = new Pawn();
        chessList[13] = new Pawn();
        chessList[14] = new Pawn();
        chessList[15] = new Pawn();

        //Assign color
        for (int i = 0; i < 16; i++) {
            chessList[i].color = color;
        }

        //Assign position
        //White pieces start from a1 to h2
        char file = 'a';
        if (color.equals("w")) {
            for (int i = 0; i < 8; i++) {
                //Assigning pieces other than pawns
                chessList[i].pos = file + "1";
                //Assigning pawns
                chessList[i + 8].pos = file + "2";
                file++;
            }
        }

        //Black pieces start from a7 to h8
        else {
            for (int i = 0; i < 8; i++) {
                //Assigning pieces other than pawns
                chessList[i].pos = file + "8";
                //Assigning pawns
                chessList[i + 8].pos = file + "7";
                file++;
            }
        }
        return chessList;
    }

    /**
     * Method to check if the target square is vacant
     * @param pos: the position of the square
     * @return the result true/false
     */
    private static boolean square_vacant(String pos) {
        for (int i = 0; i < 16; i++) {
            if (playerW[i].pos.equals(pos)) {
                return false;
            }
            if (playerB[i].pos.equals(pos)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method that returns a piece of the given position
     * @param pos: the current position
     * @return get the current piece
     */
    public static Piece get_piece(String pos)
    {
        for (int i = 0; i < 16; i++) {
            if (Chess.playerW[i].pos.equals(pos)) {
                return Chess.playerW[i];
            }
            if (Chess.playerB[i].pos.equals(pos)) {
                return Chess.playerB[i];
            }
        }
        return null;
    }

    /**
     * Method that returns a piece of the given position
     * @param pos:  String position
     * @param pawn: the pawn which is going to promote
     */
    public static void set_piece(String pos, Piece pawn) {
        for (int i = 0; i < 16; i++) {
            if (playerW[i].pos.equals(pos)) {
                playerW[i] = pawn;
            }
            if (playerB[i].pos.equals(pos)) {
                playerB[i] = pawn;
            }
        }
    }

    /**
     * Method that make pawn promotion
     * @param pawn: the pawn which is going to promote
     * @param prom: the request for the user to change the pawn to Queen(Q), Knight(N), Rook(R) or Bishop(B)
     * @return the piece after promotion
     */
    public static Piece pawnPromotion(Pawn pawn, char prom) {
        Piece promotion = null;
        switch (prom) {
            case 'R':
                promotion = new Rook();
                break;
            case 'N':
                promotion = new Knight();
                break;
            case 'B':
                promotion = new Bishop();
                break;
            case 'Q':
                promotion = new Queen();
                break;
        }
        promotion.pos = pawn.pos;
        promotion.color = pawn.color;
        promotion.steps = pawn.steps;
        return promotion;
    }

    /**
     * Method to set up and print the board
     * @param player1: Player1
     * @param player2: Player2
     */
    public static void setBoard(Piece[] player1, Piece[] player2) {
        //print out the board
        //Going through each rank
        for (int i = 0; i < 8; i++) {
            int rank = 8 - i;

            //Going thru each file
            char file = 'a';

            for (int j = 0; j < 8; j++) {
                String curr_pos = Character.toString(file++) + rank;
                //if the spot is vacant
                if (square_vacant(curr_pos)) {
                    //white square
                    if ((rank + 8 - j) % 2 == 0) {
                        System.out.print("   ");
                    }
                    //black squre
                    else {
                        System.out.print("## ");
                    }
                }
                //if the spot is not vacant
                else {
                    System.out.print(get_piece(curr_pos).toString() + " ");
                }
            }
            //Done with the for loop of the current rank
            System.out.print(rank + "\n");

        }
        //Done with the for loop of the whole board
        System.out.println(" a  b  c  d  e  f  g  h\n");
    }

    /**
     * Method to move pieces with a given command in String format i.e. "e2 e4"
     * @param move: the move position including current and destination
     * @return String of destination
     */
    public static String move_piece(String move) {
        Log.d("move is ", move);

        char promotion = ' ';
        String curr_pos, end_pos;

        if (move.contains("draw")) { return "draw"; }
        else if (move.contains("resign")) { return "resign"; }

        if ((move.length() == 7) && ((move.charAt(6) == 'R') || (move.charAt(6) == 'N') || (move.charAt(6) == 'B') || (move.charAt(6) == 'Q'))) {
            promotion = move.charAt(6);
        } else if (move.length() != 5) {
            return "invalid";
        }

        curr_pos = String.valueOf(move.substring(0, 2));
        end_pos = String.valueOf(move.substring(3, 5));

        if ((get_piece(curr_pos) != null) && (get_piece(end_pos) != null)) {
            if (get_piece(curr_pos).toString().charAt(0) == get_piece(end_pos).toString().charAt(0)) {
                return "invalid";
            }
        }

        ValidMove check = new ValidMove();
        if (!(check.checkmove(get_piece(curr_pos), move))) {
            return "invalid";
        }

        if (!(get_piece(curr_pos) instanceof Knight)) {
            if (!(get_piece(curr_pos).isClearPath(move))) {
                return "invalid";
            }
        }

        //See if its castling
        if ((move.equals("e1 g1") || move.equals("e1 c1")) || (move.equals("e8 g8") || move.equals("e8 c8"))) {
            switch (move) {
                //white king-side castling
                case "e1 g1":
                    if (Utilities.playerW[7].steps == 0) {
                        Utilities.playerW[7].updatePos("f1");
                        Utilities.playerW[4].steps++;
                        //return true;
                    }
                    break;

                //black king-side castling
                case "e8 g8":
                    if (Utilities.playerB[7].steps == 0) {
                        playerB[7].updatePos("f8");
                        playerB[4].steps++;
                    }
                    break;

                //white queen-side castling
                case "e1 c1":
                    if (Utilities.playerW[0].steps == 0) {
                        playerW[0].updatePos("d1");
                        playerW[4].steps++;
                    }
                    break;

                //black queen-side castling
                case "e8 c8":
                    if (Utilities.playerB[0].steps == 0) {
                        playerB[0].updatePos("d8");
                        playerB[4].steps++;
                        //return true;
                    }
            }
        }

        if (get_piece(end_pos) != null) {
            capture_piece(get_piece(end_pos));
        }

        /* Pawn Promotion */
        if (get_piece(curr_pos) instanceof Pawn) {
            Piece promote = promotion(get_piece(curr_pos), end_pos, promotion);
        }

        get_piece(curr_pos).updatePos(end_pos);
        get_piece(end_pos).steps++;
        testincheck();
        return end_pos;
    }

    /**
     * Method to check if the player is moving the opponent's piece
     * @param target: the captured target
     */
    public static void capture_piece(Piece target) {
        target.updatePos("z9"); //move the target out of the board
    }

    public static Piece promotion(Piece p, String end, char command) {
        Piece newPiece = null;

        if (end.charAt(1) == '1' || end.charAt(1) == '8') {
            if (command == ' ') {
                command = 'Q';
            }
            set_piece(p.pos, pawnPromotion((Pawn) p, command));
        }
        return newPiece;
    }

    /**
     * Check all possible moves of a piece on the board, including the squares occupied by oppenent's piece
     * but not the spot where the path is not clear EXCEPT FOR THE PAWN
     * @param target: Piece target
     * @return an String array
     */
    public static String[] all_moves(Piece target) {
        String[] result = new String[64];
        char rank, file;
        int i = 0;

        for (file = '1'; file <= '8'; file++) {
            for (rank = 'a'; rank <= 'h'; rank++) {

                ValidMove check = new ValidMove();

                String curr_position = Character.toString(rank) + Character.toString(file);
                if (target instanceof Pawn) { continue; }

                if (check.checkmove(target, target.pos + " " + curr_position)) {
                    if (!(target instanceof Knight)) {
                        if (target.isClearPath(target.pos + " " + curr_position)) {
                            result[i++] = Character.toString(rank) + Character.toString(file);
                        }
                    } else if (target instanceof Knight) {
                        result[i++] = Character.toString(rank) + Character.toString(file);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Run through all the pieces on the board, for each piece, check if it can make a move on the opponent's King.
     * @param square: String square
     * @return an array of Piece
     */
    public static Piece[] square_threatened(String square) {
        Piece result[] = new Piece[32];
        int result_i = 0;
        for (int i = 0; i < 16; i++) {
            String possible_square_w[] = all_moves(playerW[i]);
            String possible_square_b[] = all_moves(playerB[i]);
            for (int j = 0; j < 64; j++) {
                if (square.equals(possible_square_w[j])) {
                    result[result_i++] = playerW[i];
                }
                if (square.equals(possible_square_b[j])) {
                    result[result_i++] = playerB[i];
                }
            }
        }

        char x = square.charAt(0);
        char y = square.charAt(1);

        //check the bottom left square
        if (get_piece(Character.toString((char) (x - 1)) + Character.toString((char) (y - 1))) instanceof Pawn) {
            result[result_i++] = get_piece(Character.toString((char) (x - 1)) + Character.toString((char) (y - 1)));
        }
        //check the top left square
        if (get_piece(Character.toString((char) (x - 1)) + Character.toString((char) (y + 1))) instanceof Pawn) {
            result[result_i++] = get_piece(Character.toString((char) (x - 1)) + Character.toString((char) (y + 1)));
        }
        //check the bottom right square
        if (get_piece(Character.toString((char) (x + 1)) + Character.toString((char) (y - 1))) instanceof Pawn) {
            result[result_i++] = get_piece(Character.toString((char) (x + 1)) + Character.toString((char) (y - 1)));
        }
        //check the top right square
        if (get_piece(Character.toString((char) (x + 1)) + Character.toString((char) (y + 1))) instanceof Pawn) {
            result[result_i++] = get_piece(Character.toString((char) (x + 1)) + Character.toString((char) (y + 1)));
        }
        return result;
    }

    private static char testincheck() {
        Piece attack_b[] = square_threatened(playerW[4].pos);
        Piece attack_w[] = square_threatened(playerB[4].pos);

        for (int i = 0; i < 32; i++) {
            if (attack_b[i] != null) {
                if (attack_b[i].color.equals("b")) {
                    System.out.println("attacking: " + attack_b[i].toString() + " in " + attack_b[i].pos);
                    System.out.println("\nwhite in check");
                    return 'w';
                }
            }
            if (attack_w[i] != null) {
                if (attack_w[i].color.equals("w")) {
                    System.out.println("attacking: " + attack_w[i].toString() + " in " + attack_w[i].pos);
                    System.out.println("\nblack in check");
                    return 'b';
                }
            }
        }
        return 'n';
    }

    private static boolean checkmate() {
        Piece check_w[];
        Piece check_b[];
        int i = 0;

        if (testincheck() == 'n') { return false; }

        //white is in check
        if (testincheck() == 'w') {
            //first see if the king's surrounding is in check as well
            String[] surr = new String[8];
            char x = playerW[4].pos.charAt(0);
            char y = playerW[4].pos.charAt(1);
            //top right corner
            if (x + 1 <= 'h' && y + 1 <= '8') {
                surr[i++] = Character.toString((char) (x + 1)) + Character.toString((char) (y + 1));
            }
            //top
            if (y + 1 <= '8') {
                surr[i++] = Character.toString((char) (x + 1)) + Character.toString(y);
            }
            //top left
            if (x - 1 >= 'a' && y + 1 <= '8') {
                surr[i++] = Character.toString((char) (x - 1)) + Character.toString((char) (y + 1));
            }
            //left
            if (x - 1 >= 'a') {
                surr[i++] = Character.toString((char) (x - 1)) + Character.toString(y);
            }
            //bottom left
            if (x - 1 >= 'a' && y - 1 >= '1') {
                surr[i++] = Character.toString((char) (x - 1)) + Character.toString((char) (y - 1));
            }
            //bottom
            if (y - 1 >= '1') {
                surr[i++] = Character.toString(x) + Character.toString((char) (y - 1));
            }
            //bottom right
            if (x + 1 <= 'h' && y - 1 >= '1') {
                surr[i++] = Character.toString((char) (x + 1)) + Character.toString((char) (y - 1));
            }
            //right
            if (x + 1 <= 'h') {
                surr[i++] = Character.toString((char) (x + 1)) + Character.toString(y);
            }

            //check the top right corner is in check
            if (x + 1 <= 'h' && y + 1 <= '8') {
                check_w = square_threatened(Character.toString((char) (x + 1)) + Character.toString((char) (y + 1)));
                return check_w[0] != null;
            }
        }
        /*
        //black is in check
        if (testincheck() == 'b') {
            //first see if the king's surrounding is in check as well
            String[] surr = new String[8];
            char x = playerB[4].pos.charAt(0);
            char y = playerB[4].pos.charAt(1);
            //top right corner
            if (x + 1 <= 'h' && y + 1 <= '8') {
                surr[i++] = Character.toString((char) (x + 1)) + Character.toString((char) (y + 1));
            }
            //top
            if (y + 1 <= '8') {
                surr[i++] = Character.toString((char) (x + 1)) + Character.toString((char) (y));
            }
            //top left
            if (x - 1 >= 'a' && y + 1 <= '8') {
                surr[i++] = Character.toString((char) (x - 1)) + Character.toString((char) (y + 1));
            }
            //left
            if (x - 1 >= 'a') {
                surr[i++] = Character.toString((char) (x - 1)) + Character.toString((char) (y));
            }
            //bottom left
            if (x - 1 >= 'a' && y - 1 >= '1') {
                surr[i++] = Character.toString((char) (x - 1)) + Character.toString((char) (y - 1));
            }
            //bottom
            if (y - 1 >= '1') {
                surr[i++] = Character.toString((char) (x)) + Character.toString((char) (y - 1));
            }
            //bottom right
            if (x + 1 <= 'h' && y - 1 >= '1') {
                surr[i++] = Character.toString((char) (x + 1)) + Character.toString((char) (y - 1));
            }
            //right
            if (x + 1 <= 'h') {
                surr[i++] = Character.toString((char) (x + 1)) + Character.toString((char) (y));
            }

            //check the top right corner is in check
            if (x + 1 <= 'h' && y + 1 <= '8') {
                check_b = square_threatened(Character.toString((char) (x + 1)) + Character.toString((char) (y + 1)));
                if (check_b[0] == null) {
                    return false;
                }
            }
        }*/
        return true;
    }

    static public String intToPos(int intPos){
        String pos = null;

        char row = '1';
        char column = 'a';

        for(int i = 0; i < 64; i++){
            if(column > 'h'){
                column = 'a';
                row++;
            }
            if(i == intPos){
                return (column+"") + (row+ "");
            }
            column++;
        }
        return pos;
    }
}