package com.example.cx59.chess30;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chuanqi Xiong
 * @author Chenjie Wu
 */

public class MainActivity extends Activity {
    static List<ChessGame> GameList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent newChessGame = new Intent(MainActivity.this, ChessActivity.class);
        startActivity(newChessGame);
    }
}
